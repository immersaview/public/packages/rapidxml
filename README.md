**Nuget packaging for RapidXml Repository**

## Example CMake Usage:
```cmake
target_link_libraries(target rapidxml)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:rapidxml,INTERFACE_INCLUDE_DIRECTORIES>")
```
